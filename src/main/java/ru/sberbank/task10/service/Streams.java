package ru.sberbank.task10.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Streams<T> {

    List<T> streamsList;
    List<? super IterfaceFilter> functionalList = new ArrayList<>();

    public Streams(List<T> streamsList) {
        this.streamsList = streamsList;
    }

    public static <V> Streams<V> of(List<V> list) {
        return new Streams<V>(list);
    }

    public Streams<T> filter(MyFilter<T> functionalInterface) {
        functionalList.add(functionalInterface);
        return this;
    }

    public <V> Streams<V> transform(MyTransform<T, V> functionalInterface) {
        functionalList.add(functionalInterface);
        return (Streams<V>) this;
    }

    public <K, V> Map<K, V> toMap(MyToMap<T, K> myToMapKey, MyToMap<T, V> myToMapValue) {
        Map<K, V> map = new HashMap<>();
        V vObject = null;
        T tObjectBackup = null;
        for (T tObject : streamsList) {
            tObjectBackup = tObject;
            if (functionalList.size() > 0) {
                for (Object functional : functionalList) {
                    if (functional instanceof MyFilter) {
                        MyFilter<T> filter = (MyFilter<T>) functional;
                        if (!filter.filter(tObject)) {
                            tObjectBackup = null;
                        }
                    } else if (tObjectBackup != null && functional instanceof MyTransform) {
                        MyTransform<T, V> transform = (MyTransform<T, V>) functional;
                        vObject = (V) transform.transform(tObjectBackup);
                        tObjectBackup = (T) vObject;
                    }
                }

            }
            if (tObjectBackup != null && vObject == null)
                map.put(myToMapKey.toMap(tObjectBackup), myToMapValue.toMap(tObjectBackup));
            if (tObjectBackup != null && vObject != null)
                map.put(myToMapKey.toMap((T) vObject), myToMapValue.toMap((T) vObject));
            vObject = null;
            tObjectBackup = null;
        }

        return map;
    }


    private interface IterfaceFilter {

    }

    public interface MyFilter<T> extends IterfaceFilter {
        boolean filter(T person);
    }

    public interface MyTransform<T, V> extends IterfaceFilter {
        V transform(T person);
    }

    public interface MyToMap<T, V> extends IterfaceFilter {
        V toMap(T personKey);
    }
}