package ru.sberbank.task10;

import ru.sberbank.task10.entity.Person;
import ru.sberbank.task10.service.Streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class App {
    public static void main(String[] args) {
        Person person1 = new Person("Иван", 26);
        Person person2 = new Person("Роман", 10);
        Person person3 = new Person("Владимир", 52);
        Person person4 = new Person("Егор", 45);
        Person person5 = new Person("Сергей", 73);
        Person person6 = new Person("Николай", 54);
        Person person7 = new Person("Виктор", 35);
        List<Person> someCollection = new ArrayList<>();
        someCollection.add(person1);
        someCollection.add(person2);
        someCollection.add(person3);
        someCollection.add(person4);
        someCollection.add(person5);
        someCollection.add(person6);
        someCollection.add(person7);

        Map<String, Person> m = Streams.of(someCollection)
                .filter(p -> p.getAge() < 30)
                .transform(p -> "new Person(p.getName(), p.getAge() + 15)")
                .transform(p -> "new Person(p.getName(), p.getAge() + 16)")
                .transform(p -> "new Person(p.getName(), p.getAge() + 17)")
                .transform(p -> "new Person(p.getName(), p.getAge() + 18)")
                .transform(p -> "new Person(p.getName(), p.getAge() + 19)")
                .toMap(p -> p, p -> new Person(p, 1));


        for (Map.Entry<String, Person> entry : m.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue().toString());
        }


    }
}
